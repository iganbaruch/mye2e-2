package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        String style = "<style type='text/css' media='screen'>";
        style += "body { background-color: black; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: #ff007f; font-size: 250%; }";
        style += "</style>";
        
        String message = "It is Lulu bDay";
        message += "<p><center><img src='https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png' alt='Tanuki'        width='250'></center>";

        
        String body = "<body>" + message + "</body>";

        return style + body;
    }

}